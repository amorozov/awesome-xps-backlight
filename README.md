Display backlight adjustment for Dell XPS & Awesome WM
------------------------------------------------------

As a Linux user, I use terminals and browser all the time. Terminals have
black background, and sites in browser are usually light. So, it'd be nice
to increase backlight brightness on switch to a terminal and decrease
it when the browser is in focus. I use a dozen and a half of virtual
desktops, most of which are assigned to a specific work type: code, sys
administration, browsers, PDF datasheets, etc. So it becomes matter of
adjusting display brightness on desktop switch.

This program is tested to work on Dell XPS 9360 laptop in Linux with
tiling Awesome WM. I think it can be easily modified to work with other
types of HW. And it's not tied to WM, it just need to be called with
number of a virtual desktop as argument.
