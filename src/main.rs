use std::fmt::{Debug, Display};
use std::fs::File;
use std::io::{Read, Write};
use std::io;
use std::path::PathBuf;

const CTRL_DIR: &str = "/sys/class/backlight/intel_backlight";
const SAVE_DIR: &str = "/var/lib/backlight";
const STEPS: usize = 50;


fn file_read_num<T: std::str::FromStr + Debug>(dir: &str, name: &str) -> io::Result<T>
    where <T as std::str::FromStr>::Err: std::fmt::Debug {

    // let full_path = path.as_path_buf();
    let mut path = PathBuf::from(dir);
    path.push(name);
    let mut file = File::open(path)?;

    let mut contents = String::new();
    file.read_to_string(&mut contents)?;

    Ok(contents.trim().parse().unwrap())
}

fn file_write_num<T: Display>(dir: &str, name: &str, v: T) -> io::Result<()> {
    let mut path = PathBuf::from(dir);
    path.push(name);
    let mut file = File::create(&path)?;
    let s = format!("{}", v);

    file.write_all(s.as_bytes())?;
    Ok(())
}

fn clamp(x: f64, min: f64, max: f64) -> f64 {
    match x {
        x if x < min => min,
        x if x > max => max,
        x => x,
    }
}

fn remap(x: f64) -> f64 {
    x.powf(3.5)
}

fn set(x: f64) -> io::Result<()> {
    // let old: i32 = file_read_num(CTRL_DIR, "brightness")?;
    let max: f64 = file_read_num(CTRL_DIR, "max_brightness")?;
    let new: i32 = clamp(x * max, 1.0, max).round() as i32;

    // let diff = new - old;
    // if diff > 0 {
    //     file_write_num(CTRL_DIR, "brightness", max as i32)?;
    // } else if diff < 0 {
    //     file_write_num(CTRL_DIR, "brightness", 0)?;
    // } else {
    //     return Ok(());
    // }
    
    // sleep(Duration::from_millis(1));
    file_write_num(CTRL_DIR, "brightness", new)?;
    Ok(())
}

fn set_brightness() -> io::Result<()> {
    let desktop: usize = file_read_num(SAVE_DIR, "desktop").unwrap_or(0);
    let global: f64 = file_read_num(SAVE_DIR, "global").unwrap_or(0.0);
    let local: f64 = file_read_num(SAVE_DIR, &format!("local{}", desktop))
        .unwrap_or(0.0);
    let total = clamp(global + local, 0.0, 1.0);

    println!("set {}, {}", total, remap(total));
    set(remap(total))
}

fn adjust_global(step: i32) -> io::Result<()> {
    let current: f64 = file_read_num(SAVE_DIR, "global").unwrap_or(0.5);
    let new = clamp(current + step as f64 / STEPS as f64, -0.5, 1.5);
    file_write_num(SAVE_DIR, "global", new)?;

    set_brightness()?;
    Ok(())
}

fn set_local(x: f64) -> io::Result<()> {
    let desktop: usize = file_read_num(SAVE_DIR, "desktop").unwrap_or(0);
    let fname = format!("local{}", desktop);
    file_write_num(SAVE_DIR, &fname, x)?;

    set_brightness()?;
    Ok(())
}

fn adjust_local(step: i32) -> io::Result<()> {
    let desktop: usize = file_read_num(SAVE_DIR, "desktop").unwrap_or(0);
    let fname = format!("local{}", desktop);
    let current: f64 = file_read_num(SAVE_DIR, &fname).unwrap_or(0.0);
    let new = clamp(current + step as f64 / STEPS as f64, -0.5, 0.5);
    file_write_num(SAVE_DIR, &fname, new)?;

    set_brightness()?;
    Ok(())
}

fn switch_desktop(new: usize) -> io::Result<()> {
    let old: usize = file_read_num(SAVE_DIR, "desktop").unwrap_or(0);
    if new != old {
        file_write_num(SAVE_DIR, "desktop", new)?;
        set_brightness()?;
    }
    Ok(())
}

fn main() {
    let args: Vec<String> = std::env::args().skip(1).collect();

    if args.len() != 2 {
        return;
    }

    match &args[0] as &str {
        "adjust_global" => adjust_global(args[1].parse().unwrap()).unwrap(),
        "set_local" => set_local(args[1].parse().unwrap()).unwrap(),
        "adjust_local" => adjust_local(args[1].parse().unwrap()).unwrap(),
        "switch_desktop" => switch_desktop(args[1].parse().unwrap()).unwrap(),
        "set_raw" => set(args[1].parse().unwrap()).unwrap(),
        _ => panic!("commands: adjust_global, adjust_local, switch_desktop, set_raw"),

    }
}
